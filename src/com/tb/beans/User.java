package com.tb.beans;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class User implements ApplicationContextAware ,BeanNameAware{
	private List<Sum> sumA;
    public List<Sum> getSumA() {
		return sumA;
	}
	public void setSumA(List<Sum> sumA) {
		this.sumA = sumA;
	}
	public void showSum() {
		for(Sum sum : sumA)
		{
		System.out.println(sum.addAll());
		}
	}
	
	/*	private Sum sumB;
	private Sum sumC;*/
	private ApplicationContext context;
	
	/*public Sum getSumA() {
		return sumA;
	}
	public void setSumA(Sum sumA) {
		this.sumA = sumA;
	}
	public Sum getSumB() {
		return sumB;
	}
	public void setSumB(Sum sumB) {
		this.sumB = sumB;
	}
	public Sum getSumC() {
		return sumC;
	}
	public void setSumC(Sum sumC) {
		this.sumC = sumC;
	}
	public ApplicationContext getContext() {
		return context;
	}
	public void setContext(ApplicationContext context) {
		this.context = context;
	}
	public void showSum() {
		System.out.println(getSumA().addAll());
		System.out.println(getSumB().addAll());
		System.out.println(getSumC().addAll());
	}*/
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context=context;
	}
	@Override
	public void setBeanName(String beaname) {
		System.out.println("Beans Name ="+beaname);
	}
	public void myInit(){
		System.out.println("MyInit method");
	}
	public void myDestroy(){
		System.out.println("myDestroy method");
	}
}
